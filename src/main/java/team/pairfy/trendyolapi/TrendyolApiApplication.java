package team.pairfy.trendyolapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import team.pairfy.trendyolapi.infra.api.controller.CategoryController;

import java.sql.SQLException;

@SpringBootApplication
public class TrendyolApiApplication {
    // TODO: 26.11.2020 birden fazla nesneyi nasıl ekleyebiliriz ? (saveAll, deleteAll nasıl yaparız?)
    // TODO: 26.11.2020 başka sınıflardaan contexte nasıl ulaşırız, ulaştık diyelim bu bizim ne işimize yarar?
    // TODO: 26.11.2020 object dönmek nedir? (nesne, string)
    // TODO: 30.11.2020 wht is host? host to host, node talk to node ? whats diffrent?  

    public static void main(String[] args) throws SQLException {
        ConfigurableApplicationContext context = SpringApplication.run(TrendyolApiApplication.class, args);
        CategoryController bean = context.getBean(CategoryController.class);
        bean.category();

    }
}
//deneme