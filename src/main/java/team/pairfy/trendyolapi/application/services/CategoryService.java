package team.pairfy.trendyolapi.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import team.pairfy.trendyolapi.domain.Category;
import team.pairfy.trendyolapi.infra.api.mapper.CategoryMapper;
import team.pairfy.trendyolapi.infra.api.model.request.category.CreateCategoryRequest;
import team.pairfy.trendyolapi.infra.api.model.request.category.DeleteCategoryRequest;
import team.pairfy.trendyolapi.infra.api.model.request.category.UpdateCategoryRequest;
import team.pairfy.trendyolapi.infra.api.model.response.CategoryResponse;
import team.pairfy.trendyolapi.infra.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;


    public String createCategory(CreateCategoryRequest createCategoryRequest) {

        // to Domain
        Category category = new Category(createCategoryRequest.getName(), createCategoryRequest.getDescription());
        categoryRepository.save(category); // insert into hibernate

        return "ok";
    }

    public ResponseEntity existsCategoryById(Integer id) {

        boolean exists = categoryRepository.existsById(id);
        ResponseEntity entity;
        if (exists) {
            return entity = new ResponseEntity("Aradıgnız categori db'de mevcuttur", HttpStatus.OK);
        }
        return entity = new ResponseEntity("Aradıgınız categori db'de mevcut degildir", HttpStatus.NOT_FOUND);

    }

    public void updateCategory(UpdateCategoryRequest categoryUpdateRequest) {
        Category category = new Category(categoryUpdateRequest.getName(), categoryUpdateRequest.getDescription(), categoryUpdateRequest.getId());

        categoryRepository.save(category);
    }

    public void delete(Integer id) {
        categoryRepository.deleteById(id);
    }


    public void delete(DeleteCategoryRequest deleteCategoryRequest) {
        Category category = new Category(deleteCategoryRequest.getId());

        categoryRepository.delete(category);
    }

    public CategoryResponse getCategoryById(Integer id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);

        if (!categoryOptional.isPresent()) return null;

        Category category = categoryOptional.get();

        return CategoryMapper.map(category);
    }

    public List<CategoryResponse> getCategoryByName(String name) {
        List<Category> listOfCategoryByName = categoryRepository.findByName(name);

        return listOfCategoryByName
                .stream()
                .map(category -> new CategoryResponse(category.getId(), category.getName()))
                .collect(Collectors.toList());
    }

    public List<CategoryResponse> getCategoryByIdAndName(Integer id, String name) {
        List<Category> categoryRepositoryByIdAndName = categoryRepository.findByIdAndName(id, name);

        List<CategoryResponse> categoryResponses = categoryRepositoryByIdAndName
                .stream()
                .map(category -> new CategoryResponse(category.getId(), category.getName()))
                .collect(Collectors.toList());

        return categoryResponses;
    }

    public List<CategoryResponse> getAllCategoryWithResponse() {
        Iterable<Category> categoryRepositoryAll = categoryRepository.findAll();
        Iterator<Category> iterator = categoryRepositoryAll.iterator();
        List<Category> categoryList = new ArrayList<Category>();

        while (iterator.hasNext()) {
            categoryList.add(iterator.next());
        }

        List<Category> list = (List<Category>) categoryRepository.findAll();

        List<CategoryResponse> categoryResponses = categoryList
                .stream()
                .map(category -> new CategoryResponse(category.getId(), category.getName()))
                .collect(Collectors.toList());

        return categoryResponses;
    }

    public List<Category> findByDescription(String description) {
        return categoryRepository.findByDescription(description);
    }

    public String count() {
        return String.format("{\"count\" : %d }", categoryRepository.count());
    }


    public String createCategoryMany(List<CreateCategoryRequest> createCategoryRequestList) {
        List<Category> categories = createCategoryRequestList.stream().map(createCategoryRequest -> {
            Category category = new Category();
            category.setDescription(createCategoryRequest.getDescription());
            category.setName(createCategoryRequest.getName());
            return category; //why?
        }).collect(Collectors.toList());
        categoryRepository.saveAll(categories);

        return "ok";
    }
}
