package team.pairfy.trendyolapi.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team.pairfy.trendyolapi.domain.Supplier;
import team.pairfy.trendyolapi.infra.api.mapper.SupplierMapper;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.CreateSupplierRequest;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.DeleteSupplierRequest;
import team.pairfy.trendyolapi.infra.api.model.request.supplier.UpdateSupplierRequest;
import team.pairfy.trendyolapi.infra.api.model.response.SupplierResponse;
import team.pairfy.trendyolapi.infra.repository.SupplierRepository;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SupplierService {

    @Autowired
    SupplierRepository supplierRepository;

    public Supplier create(CreateSupplierRequest createSupplierRequest) {
        Supplier supplier = SupplierMapper.map(createSupplierRequest);

        return supplierRepository.save(supplier);
    }

    public Supplier update(UpdateSupplierRequest updateSupplierRequest) {
        Supplier supplier = SupplierMapper.map(updateSupplierRequest);

        return supplierRepository.save(supplier);
    }

    public List<SupplierResponse> getSupplierByCountry(String country) {
        List<Supplier> suppliers = supplierRepository.findByCountry(country);

        return suppliers.stream().map(supplier -> new SupplierResponse(supplier.getId(), supplier.getCity())).collect(Collectors.toList());
    }

    public List<SupplierResponse> getSupplierByName(String name) {
        List<Supplier> suppliers = supplierRepository.findByName(name);

        return suppliers.stream().map(supplier -> new SupplierResponse(supplier.getId(), supplier.getCity())).collect(Collectors.toList());
    }

    public List<SupplierResponse> getSupplierByCityandName(String city, String name) {
        List<Supplier> suppliers = supplierRepository.findByCityAndName(city, name);

        return suppliers.stream().map(supplier -> new SupplierResponse(supplier.getId(), supplier.getCity())).collect(Collectors.toList());
    }

    public List<SupplierResponse> getSupplierByCity(String city) {
        List<Supplier> suppliers = supplierRepository.findByCity(city);

       return suppliers.stream().map(supplier -> new SupplierResponse(supplier.getId(), supplier.getCity())).collect(Collectors.toList());
    }

    public SupplierResponse getCategoryById(Integer id) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(id);

        if (!supplierOptional.isPresent()) return null;

        Optional<SupplierResponse> supplierResponse = supplierOptional
                .map(supplier -> new SupplierResponse(supplier.getId(), supplier.getCity()));

        return supplierResponse.get();
    }

    public void deleteById(DeleteSupplierRequest deleteSupplierRequest) {
        supplierRepository.deleteById(deleteSupplierRequest.getId());
    }

    public String countByName(String name) {
        //var ama kaç tane var?
        int count = 0;

        Iterable<Supplier> supplierRepositoryAll = supplierRepository.findAll();
        Iterator<Supplier> supplierIterator = supplierRepositoryAll.iterator();

        while (supplierIterator.hasNext())
            if (supplierIterator.next().getName().equals(name))
                ++count;

        return String.format("Aradıgınız isim database'de %d'tane bulundu", count);
    }

    public String count() {
        return String.format("{\"count\" : %d }", supplierRepository.count());
    }
}
