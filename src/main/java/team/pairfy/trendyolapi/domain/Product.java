package team.pairfy.trendyolapi.domain;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;

    private int supplierId;
    private String unit;
    private double price;

    @ManyToOne
    // @JoinColumn("category_id")
    private Category category;

    @ManyToOne
    private ProductContext productContext;

    public Product() {
    }

    public Product(String name, int supplierId, String unit, double price) {
        this.name = name;
        this.supplierId = supplierId;
        this.unit = unit;
        this.price = price;
    }

    public Product(int id, String name, int supplierId, String unit, double price) {
        this.id = id;
        this.name = name;
        this.supplierId = supplierId;
        this.unit = unit;
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }


    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public String getUnit() {
        return unit;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", supplierId=" + supplierId +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                '}';
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ProductContext getProductContext() {
        return productContext;
    }

    public void setProductContext(ProductContext productContext) {
        this.productContext = productContext;
    }
}
