package team.pairfy.trendyolapi.domain;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "productcontext")
public class ProductContext {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @ManyToOne
    private ProductContextVariant productContextVariant;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<Product> productList;

    public ProductContext() {
    }

    public ProductContext(int id, String name, List<Product> productList) {
        this.id = id;
        this.name = name;
        this.productList = productList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public ProductContextVariant getProductContextVariant() {
        return productContextVariant;
    }

    public void setProductContextVariant(ProductContextVariant productContextVariant) {
        this.productContextVariant = productContextVariant;
    }
}
