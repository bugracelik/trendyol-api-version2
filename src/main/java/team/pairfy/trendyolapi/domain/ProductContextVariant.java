package team.pairfy.trendyolapi.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "productcontextvariant")
public class ProductContextVariant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @OneToMany(mappedBy = "productcontext", cascade = CascadeType.ALL)
    private List<ProductContext> productContextList;

    public ProductContextVariant(int id, String name, List<ProductContext> productContextList) {
        this.id = id;
        this.name = name;
        this.productContextList = productContextList;
    }


    public ProductContextVariant() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductContext> getProductContextList() {
        return productContextList;
    }

    public void setProductContextList(List<ProductContext> productContextList) {
        this.productContextList = productContextList;
    }
}
