package team.pairfy.trendyolapi.infra.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.pairfy.trendyolapi.application.services.CustomerService;
import team.pairfy.trendyolapi.domain.Customer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.CreateCustomer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.DeleteCustomer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.UpdateCustomer;
import team.pairfy.trendyolapi.infra.api.model.response.CustomerResponse;

import java.util.List;

@RequestMapping("customer")
@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/getAllCustomer")
    public List<CustomerResponse> getAllCustomer() {
        return customerService.getAllCustomer();
    }

    @PostMapping("/create")
    public String createCustomer(@RequestBody CreateCustomer createCustomer) {
        return customerService.createCustomer(createCustomer);
    }

    @PutMapping("/update")
    public Customer updateCustomer(@RequestBody UpdateCustomer updateCustomer) {
        return customerService.updateCustomer(updateCustomer);
    }

    @DeleteMapping("/delete")
    public void deleteCustomer(@RequestBody DeleteCustomer deleteCustomer) {
        customerService.deleteCustomer(deleteCustomer);
    }
}

