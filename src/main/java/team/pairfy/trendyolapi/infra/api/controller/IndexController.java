package team.pairfy.trendyolapi.infra.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class IndexController {

    @GetMapping
    public void swagger(HttpServletResponse response) throws IOException {
        response.sendRedirect("swagger-ui.html");
    }
}
