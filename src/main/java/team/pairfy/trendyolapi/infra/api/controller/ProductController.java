package team.pairfy.trendyolapi.infra.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.pairfy.trendyolapi.application.services.ProductService;
import team.pairfy.trendyolapi.domain.Product;
import team.pairfy.trendyolapi.infra.api.model.request.product.CreateProductRequest;
import team.pairfy.trendyolapi.infra.api.model.request.product.DeleteProductRequest;
import team.pairfy.trendyolapi.infra.api.model.request.product.UpdateProductRequest;
import team.pairfy.trendyolapi.infra.api.model.response.ProductResponse;

import java.util.List;

@RequestMapping("product")
@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/getAllProduct")
    public List<ProductResponse> getAllProduct() {
        return productService.getAllProduct();
    }

    @GetMapping("/getByName/{name}")
    public List<ProductResponse> getByName(@PathVariable String name) {
       return productService.getByName(name);
    }

    @PostMapping("create")
    public Product create(@RequestBody CreateProductRequest createProductRequest) {
        return productService.create(createProductRequest);
    }

    @PutMapping("update")
    public Product update(@RequestBody UpdateProductRequest updateProductRequest) {
       return productService.update(updateProductRequest);
    }

    @DeleteMapping("delete")
    public Object delete(@RequestBody DeleteProductRequest deleteProductRequest) {
       return productService.delete(deleteProductRequest);
    }

}
