package team.pairfy.trendyolapi.infra.api.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import team.pairfy.trendyolapi.application.services.UserService;
import team.pairfy.trendyolapi.domain.User;
import team.pairfy.trendyolapi.infra.api.mapper.UserMapper;
import team.pairfy.trendyolapi.infra.api.model.response.UserResponse;
import team.pairfy.trendyolapi.infra.repository.UserRepository;
import team.pairfy.trendyolapi.infra.reset.model.response.GetAllUserResponse;

import java.util.List;

@RequestMapping("user")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @GetMapping("getAllUsers")
    public ResponseEntity getAllUsers() throws JsonProcessingException {
        List<User> allUsers = userService.getAllUsers();
        List<GetAllUserResponse> getAllUserResponses = UserMapper.userMap(allUsers);
        return UserResponse.response(getAllUserResponses);
    }

}
