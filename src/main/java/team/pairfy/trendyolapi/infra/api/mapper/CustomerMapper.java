package team.pairfy.trendyolapi.infra.api.mapper;

import team.pairfy.trendyolapi.domain.Customer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.CreateCustomer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.DeleteCustomer;
import team.pairfy.trendyolapi.infra.api.model.request.customer.UpdateCustomer;

public class CustomerMapper {
    public static Customer map(CreateCustomer createCustomer) {
        Customer customer = new Customer();

        customer.setAdress(createCustomer.getAdress());
        customer.setCity(createCustomer.getCity());
        customer.setEmail(createCustomer.getEmail());
        customer.setName(createCustomer.getName());
        customer.setPassword(createCustomer.getPassword());
        customer.setSurname(createCustomer.getSurname());

        return customer;
    }

    public static Customer map(UpdateCustomer updateCustomer) {
        Customer customer = new Customer();

        customer.setId(updateCustomer.getId());
        customer.setAdress(updateCustomer.getAdress());
        customer.setCity(updateCustomer.getCity());
        customer.setEmail(updateCustomer.getEmail());
        customer.setName(updateCustomer.getName());
        customer.setPassword(updateCustomer.getPassword());
        customer.setSurname(updateCustomer.getSurname());

        return customer;
    }

    public static Customer map(DeleteCustomer deleteCustomer) {
        Customer customer = new Customer();

        customer.setId(deleteCustomer.getId());
        customer.setName(deleteCustomer.getName());
        customer.setEmail(deleteCustomer.getEmail());

        return customer;

    }
}
