package team.pairfy.trendyolapi.infra.api.mapper;

import team.pairfy.trendyolapi.domain.Product;
import team.pairfy.trendyolapi.infra.api.model.request.product.CreateProductRequest;
import team.pairfy.trendyolapi.infra.api.model.request.product.UpdateProductRequest;
import team.pairfy.trendyolapi.infra.api.model.response.ProductResponse;

public class ProductMapper {

    public static Product map(CreateProductRequest createProductRequest) {
        String name = createProductRequest.getName();
        double price = createProductRequest.getPrice();
        Integer supplier_id = createProductRequest.getSupplier_id();
        String unit = createProductRequest.getUnit();

        return new Product(name, supplier_id, unit, price);

    }

    public static Product map(UpdateProductRequest updateProductRequest) {
        int id = updateProductRequest.getId();
        String name = updateProductRequest.getName();
        double price = updateProductRequest.getPrice();
        Integer supplier_id = updateProductRequest.getSupplierId();
        String unit = updateProductRequest.getUnit();

        return new Product(id, name, supplier_id, unit, price);
    }

    public static ProductResponse map(Product product) {
        ProductResponse productResponse = new ProductResponse();

        productResponse.setName(product.getName());
        productResponse.setPrice(product.getPrice());
        productResponse.setUnit(product.getUnit());

        return productResponse;
    }
}
