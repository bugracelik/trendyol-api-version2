package team.pairfy.trendyolapi.infra.api.model.request.category;

public class DeleteCategoryRequest {
    private Integer id;

    public DeleteCategoryRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
