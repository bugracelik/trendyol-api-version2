package team.pairfy.trendyolapi.infra.api.model.request.customer;

public class CreateCustomer {
    private String name;
    private String surname;
    private String adress;
    private String city;
    private String email;
    private String password;

    public CreateCustomer() {
    }

    public CreateCustomer(String name, String surname, String adress, String city, String email, String password) {
        this.name = name;
        this.surname = surname;
        this.adress = adress;
        this.city = city;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
