package team.pairfy.trendyolapi.infra.api.model.request.product;

public class UpdateProductRequest {
    private int id;
    private String name;
    private int supplierId;
    private String unit;
    private double price;

    public UpdateProductRequest() {
    }

    public UpdateProductRequest(int id, String name, int supplierId, String unit, double price) {
        this.id = id;
        this.name = name;
        this.supplierId = supplierId;
        this.unit = unit;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
