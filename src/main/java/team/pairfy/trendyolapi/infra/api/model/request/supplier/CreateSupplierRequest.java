package team.pairfy.trendyolapi.infra.api.model.request.supplier;

public class CreateSupplierRequest {
    private String address;
    private String city;
    private String country;
    private String name;

    public CreateSupplierRequest() {
    }

    public CreateSupplierRequest(String address, String city, String country, String name) {
        this.address = address;
        this.city = city;
        this.country = country;
        this.name = name;
    }

    public String getAdress() {
        return address;
    }

    public void setAdress(String adress) {
        this.address = adress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
