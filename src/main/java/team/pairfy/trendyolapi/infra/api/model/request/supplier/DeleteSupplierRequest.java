package team.pairfy.trendyolapi.infra.api.model.request.supplier;

public class DeleteSupplierRequest {
    private Integer id;
    private String name;

    public DeleteSupplierRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
