package team.pairfy.trendyolapi.infra.api.model.response;

public class CustomerResponse {
    private String name;
    private String surname;
    private String city;
    private String email;

    public CustomerResponse() {
    }

    public CustomerResponse(String name, String surname, String city, String email) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
