package team.pairfy.trendyolapi.infra.config;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class SampleConfig { // SPRING BEAN

    public void config()
    {
        System.out.println("component sample bean");
    }
}


