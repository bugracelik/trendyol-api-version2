package team.pairfy.trendyolapi.infra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.trendyolapi.domain.Category;
import team.pairfy.trendyolapi.domain.Supplier;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Integer> {
    List<Category> findByName(String name);
    List<Category> findByDescription(String description);
    List<Category> findByIdAndName(Integer id, String name);
}