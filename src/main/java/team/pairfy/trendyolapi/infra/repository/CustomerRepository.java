package team.pairfy.trendyolapi.infra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.trendyolapi.domain.Customer;

@Repository
public interface CustomerRepository extends CrudRepository <Customer, Integer> { //CustomerRepositoryImpl spring tarafıdan olusturulacak
}
