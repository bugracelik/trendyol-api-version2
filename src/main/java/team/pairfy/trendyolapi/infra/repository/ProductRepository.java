package team.pairfy.trendyolapi.infra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.trendyolapi.domain.Product;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository <Product, Integer> {
   List<Product> findByName(String name);//ProductRepositoryImpl spring tarafından oluşturalacak
   Optional<Product> findById(Integer id);

}
