package team.pairfy.trendyolapi.infra.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.pairfy.trendyolapi.domain.Supplier;

import java.util.List;
import java.util.Optional;

@Repository
public interface SupplierRepository extends CrudRepository<Supplier, Integer> { // CategoryRepositoryImpl

    // özel metotlar

    // select * from suppliers where citys = '%s'
    List<Supplier> findByCity(String city); // find suppliers by city
    List<Supplier> findByCityOrNameOrderByName(String city, String name); // find suppliers by city
    List<Supplier> findByCountry(String country); // find suppliers by city
    List<Supplier> findByName(String city); // find suppliers by city
    List<Supplier> findByCityAndName(String city, String name);
}